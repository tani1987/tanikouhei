package com.hookah.ec.webapp.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IAction {

	public void perform(HttpServletRequest req, HttpServletResponse res)
			throws Exception;
}
