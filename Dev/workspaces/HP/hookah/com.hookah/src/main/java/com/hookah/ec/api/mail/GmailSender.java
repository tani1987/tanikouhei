package com.hookah.ec.api.mail;

import static javax.mail.Transport.send;
import static javax.mail.internet.MimeUtility.encodeWord;

import java.security.Security;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class GmailSender {

	private Session session;
	private String senderAddress;
	private String charset = "iso-2022-jp";

	public GmailSender() {
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		Properties props = System.getProperties();
		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", "465");
		props.setProperty("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.auth", "true");
		this.session = getSession(props);
	}

	private Session getSession(Properties props) {
		return Session.getDefaultInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication("tani19870516@gmail.com", "tani_1987_");
			}
		});
	}

	public boolean sendMail(String address, String title, String body, String attachFilePath) {

		try {
			MimeMessage mm = new MimeMessage(session);

			// sender information
			mm.setFrom(new InternetAddress(senderAddress, "WOODSMILEPROJECT EC", charset));

			mm.addRecipients(Message.RecipientType.TO, address);
			mm.setSubject(title, charset);

			// body
			Multipart mp = new MimeMultipart();
			MimeBodyPart message = new MimeBodyPart();
			message.setText(body, charset);
			message.setHeader("Content-Type", "text/plain");
			mp.addBodyPart(message);

			if (attachFilePath != null && !"".equals(attachFilePath)) {
				FileDataSource fds = new FileDataSource(attachFilePath);
				DataHandler dh = new DataHandler(fds);
				MimeBodyPart attachment = new MimeBodyPart();
				attachment.setDataHandler(dh);
				attachment.setFileName(encodeWord(attachFilePath));
				mp.addBodyPart(attachment);
			}
			mm.setContent(mp);
			mm.setSentDate(new Date());
			send(mm);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

}
