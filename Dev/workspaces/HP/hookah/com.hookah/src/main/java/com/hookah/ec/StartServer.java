package com.hookah.ec;

import java.io.File;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppContext;

import com.hookah.ec.webapp.ActionDispacher;

public class StartServer {

	public static void main(String[] args) throws Exception {

		Server server = new Server(8080);

		WebAppContext webappcontext = new WebAppContext();
		webappcontext.setContextPath("/hookah");

		File warPath = new File(".", "src/main/java/com/hookah/ec/webapp/htdocs");
		webappcontext.setWar(warPath.getAbsolutePath());

		HandlerList handlers = new HandlerList();
		webappcontext.addServlet(new ServletHolder(new ActionDispacher()),
				"/home");
		// URL is http://localhost:8080/hookah/home
		handlers.setHandlers(new Handler[] { webappcontext,
				new DefaultHandler() });
		server.setHandler(handlers);
		server.start();
		server.join();

	}

}
