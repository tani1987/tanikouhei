package com.hookah.ec.webapp.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoOrderAction implements IAction {

	public void perform(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		req.getRequestDispatcher("/jsp/order.jsp").forward(req, res);
	}

}
