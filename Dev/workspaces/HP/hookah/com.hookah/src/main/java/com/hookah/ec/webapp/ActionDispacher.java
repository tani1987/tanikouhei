package com.hookah.ec.webapp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hookah.ec.webapp.action.BuyAction;
import com.hookah.ec.webapp.action.GoOrderAction;
import com.hookah.ec.webapp.action.IAction;
import com.hookah.ec.webapp.action.IndexAction;

public class ActionDispacher extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doGet(req, res);
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		try {
			res.setContentType("text/html;charset=UTF-8");
			String key = (String) req.getParameter("a");

			IAction action = getAction(key);
			action.perform(req, res);

		} catch (Exception e) {
			e.printStackTrace();
			dispachErrorPage(req, res);
		}

	}

	private IAction getAction(String key) {

		// new class by key
		// make factory pattern later
		if ("order".equals(key)) {
			return new GoOrderAction();
		} else if ("done".equals(key)) {
			return new BuyAction();
		} else {
			return new IndexAction();
		}

	}

	private void dispachErrorPage(HttpServletRequest req,
			HttpServletResponse res) {

		try {
			req.getRequestDispatcher("/jsp/error.jsp").forward(req, res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
