package com.hookah.ec.webapp.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hookah.ec.api.mail.GmailSender;

public class BuyAction implements IAction {

	public void perform(HttpServletRequest req, HttpServletResponse res)
			throws Exception {

		String name = req.getParameter("n");
		String caption = req.getParameter("f");
		String email = req.getParameter("e");
		String set = req.getParameter("s");
		String z1 = req.getParameter("z1");
		String z2 = req.getParameter("z2");
		String addres = req.getParameter("ad");

		String comment = req.getParameter("c");
		String memo = req.getParameter("m");

		new GmailSender().sendMail(
				"woodsmileproject.ec@gmail.com",
				"裏方兄弟発注書",
				createBody(name, caption, email, set, z1, z2, addres, comment,
						memo), null);

		new GmailSender().sendMail(
				email,
				"【WoodSmileProject EC】注文完了メール",
				createMsg(name, caption, email, set, z1, z2, addres, comment,
						memo), null);

		req.getRequestDispatcher("/jsp/done.jsp").forward(req, res);
	}

	private String createMsg(String name, String caption, String email,
			String set, String z1, String z2, String address, String comment,
			String memo) {

		StringBuffer msg = new StringBuffer();

		msg.append(name + "様");
		msg.append("\r\n");
		msg.append("\r\n");

		msg.append("ご注文ありがとうございました。");
		msg.append("\r\n");

		msg.append("お支払方法に関しましてはこちらから追って連絡させていただきます。");
		msg.append("\r\n");

		msg.append("以下の内容でご注文を受け付けました。");
		msg.append("\r\n");
		msg.append("\r\n");
		msg.append(createBody(name, caption, email, set, z1, z2, address,
				comment, memo));

		msg.append("\r\n");
		msg.append("\r\n");
		msg.append("-----------------------------------");
		msg.append("\r\n");
		msg.append("Wood Smile Project EC");
		msg.append("\r\n");
		msg.append("Email:woodsmileproject.ec@gmail.com");
		msg.append("\r\n");
		msg.append("-----------------------------------");
		msg.append("\r\n");

		return msg.toString();
	}

	private String createBody(String name, String caption, String email,
			String set, String z1, String z2, String address, String comment,
			String memo) {

		StringBuffer body = new StringBuffer();

		body.append("氏名:");
		body.append(name);
		body.append("\r\n");
		body.append("ふりがな:");
		body.append(caption);
		body.append("\r\n");
		body.append("Eメールアドレス:");
		body.append(email);
		body.append("\r\n");
		body.append("セット数:");
		body.append(set);
		body.append("\r\n");
		body.append("郵便番号:");
		body.append(z1 + "-" + z2);
		body.append("\r\n");
		body.append("住所:");
		body.append(address);
		body.append("\r\n");
		body.append("吹き出し:");
		body.append(comment);
		body.append("\r\n");
		body.append("備考:");
		body.append(memo);
		body.append("\r\n");
		body.append("小計:");
		body.append(String.valueOf(calcSub(Integer.valueOf(set).intValue())) + "円");
		body.append("\r\n");
		body.append("送料:");
		body.append(String.valueOf(calcDelivaryFee(Integer.valueOf(set).intValue())) + "円");
		body.append("\r\n");
		body.append("合計:");
		body.append(String.valueOf(calcSum(Integer.valueOf(set).intValue())) + "円");
		body.append("\r\n");

		return body.toString();
	}

	private int calcSub(int num) {

		return num * 600;

	}

	private int calcDelivaryFee(int num) {

		return num * 50;

	}

	private int calcSum(int num) {

		return num * 650;

	}

}
