package com.hookah.ec.webapp.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IndexAction implements IAction {

	public void perform(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		req.getRequestDispatcher("/html/index.html").forward(req, res);
	}

}
