<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Wood Smile Project Ec</title>
<style type="text/css">
</style>

<style type="text/css">
.back {
	background-image: url(img/background.jpg);
}

div.desp {
	text-align: left;
	width: 250px;
	font-size: 17;
	font-weight: bold;
	width: 250px;
}

img.sample-big {
	width: 400px;
	height: 500px;
}

img.sample-small {
	width: 100px;
	height: 100px;
}
</style>

</head>
<body>
	<div class="desp">wood smile project ec site</div>
	<img src="img/urakata_logo_top.jpg">
	<br>
	<br>
	<form method="POST" action="./home">
		<input type="hidden" name="a" value="done">
		<table>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">氏名</div></th>
				<th align="left"><input id="n" style="width: 500px" type="text"
					value="" name="n"></th>
			</tr>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">ふりがな</div></th>
				<th align="left"><input id="f" style="width: 500px" type="text"
					value="" name="f"></th>
			</tr>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">メールアドレス</div></th>
				<th align="left"><input id="e" style="width: 500px" type="text"
					value="" name="e"></th>
			</tr>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">セット数
						(2体で1セットになります。)</div></th>
				<th align="left"><select id="set-num" name="s">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
				</select></th>
			</tr>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">郵便番号</div></th>
				<th align="left"><input id="z1" type="text" value=""
					maxlength="3" style="width: 30px" name="z1"> - <input
					id="z2" type="text" value="" maxlength="4" style="width: 40px"
					name="z2"></th>
			</tr>

			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">住所</div></th>
				<th align="left"><input id="ad" style="width: 500px"
					type="text" value="" name="ad"></th>
			</tr>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">吹き出し</div></th>
				<th align="left"><textarea rows="30" cols=""
						style="width: 500px" name="c">（1セットにつき2つ）
（縦書き、7文字×3行程度を目安にお願いいたします。
 CDなら「NOW PLAYING」や「再生中」、本なら「今売れてます」「ベストセラー」などがオススメです。）
 セリフを空欄にする場合は、「セリフなし」とお書きください。
					</textarea></th>
			</tr>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">備考</div></th>
				<th align="left"><textarea rows="30" cols=""
						style="width: 500px" name="m"></textarea></th>
			</tr>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">小計 </div></th>
				<th align="left"><div id="sub">\600</div></th>
			</tr>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">送料 (1セットにつき50円)</div></th>
				<th align="left"><div id="dcharge">\50</div></th>
			</tr>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15">合計</div></th>
				<th align="left"><div id="sum">\650</div></th>
			</tr>
			<tr>
				<th style="width: 100px"></th>
				<th align="left"><div style="font-size: 15"></div></th>
				<th align="left"><input id="btn-ord" name="btn-ord"
					type="submit" value="注文する" /></th>
			</tr>

		</table>
	</form>
	<br>
	<br>

	<div></div>
	<img src="img/urakata_logo_bottom.jpg">

	<script type="text/javascript" charset="UTF-8" src="js/jquery-1.7.2.js"></script>
	<script type="text/javascript" charset="UTF-8" src="js/ord-ctr.js"></script>
</body>
</html>